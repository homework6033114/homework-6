class GameField {
  constructor(state, mode, gameFieldStatus, isOverGame) {
    this.state = [null, null, null, null, null, null, null, null, null]
    this.mode = 'x'
    this.gameFieldStatus = ''
    this.isOverGame = false
  }

  setMode() {
    this.mode === 'x' ? (this.mode = 'o') : (this.mode = 'x')
  }

  getGameFieldStatus() {
    const checkWinner = (cells) => {
      const winCombinations = [
        // Rows
        ['0', '1', '2'],
        ['3', '4', '5'],
        ['6', '7', '8'],
        // Columns
        ['0', '3', '6'],
        ['1', '4', '7'],
        ['2', '5', '8'],
        // Diagonal
        ['0', '4', '8'],
        ['2', '4', '6'],
      ]

      for (let comb of winCombinations) {
        if (
          cells[comb[0]] !== null &&
          cells[comb[0]] === cells[comb[1]] &&
          cells[comb[1]] === cells[comb[2]]
        ) {
          return cells[comb[0]]
        }
      }
      return false
    }

    const checkTie = (cells) => {
      for (let cell of cells) {
        if (cell === null) {
          return false
        }
      }
      return true
    }

    const winner = checkWinner(this.state)

    if (winner) {
      return (this.gameFieldStatus = `${
        winner === 'x' ? 'Крестики победили' : 'Нолики победили'
      }`)
    } else if (checkTie(this.state)) {
      return (this.gameFieldStatus = 'Ничья')
    }
  }

  getFieldCellValue(i) {
    this.state[i] = this.mode
  }
}

const gameField = new GameField()

while (!gameField.isOverGame) {
  let clickedCellIndex = +prompt('Введите номер клетки (от 0 до 8)')

  if (gameField.state[clickedCellIndex] !== null) {
    alert('Клетка занята. Введите другой номер клетки (0 -8 )')
  }
  gameField.getFieldCellValue(clickedCellIndex)
  gameField.getGameFieldStatus()

  if (gameField.gameFieldStatus) {
    gameField.isOverGame = true
  } else {
    gameField.setMode()
  }
}

alert(gameField.gameFieldStatus)
